package com.itersdesktop.javatechs;

import org.apache.log4j.Logger;

/**
 * Created by Tung on 06/07/2017.
 */
public class ArrayAverage {
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(ArrayAverage.class.getName());

    public static void main(String[] args) {
        int[] numbers = new int[]{20, 30, 25, 35, -16, 60, -100};
        //calculate sum of all array elements
        int sum = 0;
        for(int i=0; i < numbers.length ; i++)
            sum = sum + numbers[i];
        //calculate average value
        double average = sum / numbers.length;
        log.debug("Average value of the array elements is : " + average);
        log.info("Finished!");
    }
}
