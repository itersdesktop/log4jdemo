package com.itersdesktop.javatechs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tnguyen on 10/01/17.
 */
public class SimpleStatistics {
    /* the class logger */
    private static final Log log = LogFactory.getLog(SimpleStatistics.class.getName());
//    private static final Log log = LogFactory.getLog(SimpleStatistics.getClass());
//    private final Log log = LogFactory.getLog(this.getClass());

    public void SayHi(String name) {
        System.out.println(SimpleStatistics.class); // class com.itersdesktop.javatechs.SimpleStatistics
        System.out.println(this.getClass());        // class com.itersdesktop.javatechs.SimpleStatistics
        System.out.println(SimpleStatistics.class.getName()); // com.itersdesktop.javatechs.SimpleStatistics
        String helloMessage = "Hi " + name;
        System.out.println(helloMessage);
        log.debug(helloMessage);
        log.info(helloMessage);
        log.info("Finished!");
    }
}
