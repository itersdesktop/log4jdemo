package com.itersdesktop.javatechs;


/**
 * Read SBML document
 */
public class App {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        /**
         * Test log4j, commons-logging
         */
        SimpleStatistics statistics = new SimpleStatistics();
        statistics.SayHi("Khang");
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("It took ".concat(String.valueOf(duration/1000000/1000)).concat(" seconds"));
    }
}
